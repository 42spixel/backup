# **Backup**

Backup storage to AWS S3 for Docker.

### **Credits**

+ [wetransform-os/dockup](https://github.com/wetransform-os/dockup.git)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/backup/src/master/LICENSE.md)**.
